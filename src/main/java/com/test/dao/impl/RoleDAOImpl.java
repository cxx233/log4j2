package com.test.dao.impl;

import com.test.dao.IRoleDAO;
import com.test.entity.UserEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author: xujiaxi
 * @date: 2017/12/28
 */
@Component
public class RoleDAOImpl implements IRoleDAO{
    private Logger logger = LoggerFactory.getLogger(RoleDAOImpl.class);


    @Resource(name = "mySessionFactory")
    private  SessionFactory sessionFactory;

    @Override
    public boolean findAll() {
        int j=1000000;
        for(int i = 0;i<j;i++){
            logger.info("测试findAll");
            logger.error("测试=======================================================================");
        }
        UserEntity user = new UserEntity();
//        user.setId(5);
        user.setUserName("ce");
        user.setPassword("dd");


        Session session = sessionFactory.openSession();

        session.save(user);
        session.beginTransaction().commit();
//        sessionFactory.getCurrentSession().save(user);
        return false;
    }
}
