package com.test.dao.impl;

import com.test.dao.IAdminDAO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author: xujiaxi
 * @date: 2017/12/28
 */
@Component
public class AdminDAOImpl implements IAdminDAO{
//    @Resource(name = "sessionFactorySqlServer")
//    private SessionFactory sessionFactory;

    @Override
    public boolean findLogin() {
        System.out.println("【IAdminDAO】public boolean findLogin()");
        return false;
    }
}
