package com.test.action;

import com.opensymphony.xwork2.ActionSupport;
import com.test.service.IAdminService;

import javax.annotation.Resource;

/**
 * dd
 *
 * @author: xujiaxi
 * @date: 2017/12/28
 */
public class TestAction extends ActionSupport{
        @Resource
        private IAdminService adminService;

        public String testAction() {
            adminService.login();
            adminService.testLogin();
            return ActionSupport.SUCCESS;
        }

}
