package com.test.action;

import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.convention.annotation.*;
import org.springframework.stereotype.Controller;


/**
 * @author: xujiaxi
 * @date: 2018/1/2
 */

@ParentPackage("struts-default")
@Namespace("/localweb")
@Controller
//@Action(value = "test",results = @Result(name="success",location="/success.jsp"))
public class OtherAction  extends ActionSupport {

    @Action(value="test1",results = @Result(name="success",location="/success.jsp"))
    public String test1() {
        System.out.println("test1");

        return SUCCESS;
    }

    @Action(value="test2",results = @Result(name="success",location="/success.jsp"))
    public String test2() {
        System.out.println("test2");
        return SUCCESS;
    }
}
