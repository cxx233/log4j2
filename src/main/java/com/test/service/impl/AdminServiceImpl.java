package com.test.service.impl;

import com.test.dao.IAdminDAO;
import com.test.dao.IRoleDAO;
import com.test.service.IAdminService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author: xujiaxi
 * @date: 2017/12/28
 */
@Service
public class AdminServiceImpl implements IAdminService{
    @Resource
    private IAdminDAO A;

    @Resource
    private IRoleDAO B;

    @Override
    public boolean login() {
        this.A.findLogin();
        return false;
    }

    public boolean testLogin() {
        this.B.findAll();
        return false;
    }

}
