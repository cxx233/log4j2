package com.test.All;

import com.test.service.IAdminService;
import javafx.application.Application;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

/**
 * @author: xujiaxi
 * @date: 2017/12/28
 */

public class TestAdmin {
    public static void main(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("bean1.xml");
        IAdminService service = ctx.getBean("adminServiceImpl",IAdminService.class);
        service.login();
    }
}
